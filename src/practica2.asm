.data

op1:  .word 0x00000000 0x00000000 0x00000000 0x7fff0000
op2:  .word 0x00000000 0x00000000 0x00000000 0xFfff0000
resul: .space 512


.text

main:
	la $a0,op1
	la $a1,op2
	la $a2,resul
	jal suma
	j salida


mantisa:
	lhu $t0,12($a0)
	addi $t0,$t0,0x00010000
	move $v0, $t0
	lhu $t0,12($a1)
	addi $t0,$t0,0x00010000
	move $v1, $t0
	jr $ra
	
exponente:
	lhu $t1,14($a0)
	lhu $t2,14($1)
	li $t3 0x7fff
	and $t5 $t3 $t1
	move $v0 $t5
	and $t6 $t3 $t2	
	move $v1 $t6
	jr $ra
signo:
	lbu $t1,15($a0)
	lbu $t2,15($a1)
	srl $t4,$t1,7
	srl $t5,$t2,7
	move $v0 $t4
	move $v1 $t5
	jr $ra

suma:
	addi $sp $sp -4
	sw	$ra 0($sp)
       
        jal signo
	move $s0, $v0         #Primero
	move $s1, $v1	      #segundo
	
	jal exponente
	move $s2, $v0         #primero
	move $s3, $v1	      #segundo

	
	jal expouno
	move $s5, $v0
	jal mantisauno
	move $s6, $v0
	
	jal expodos
	move $s7, $v0
	jal mantisados
	move $s4, $v0

	j excepciones

	expouno:
	beq $s2,0x7fff 	unoexponente
	beq $s2,0x0000  ceroexponente
	li $t0, 2
	
	expodos:
	beq $s3,0x7fff unoexponente	
	beq $s3,0x0000 ceroexponente
	li $t1, 2
	
mantisauno:
 
	lhu $t7,12($a0)
	addi $t7,$t7,0x00010000
	move $v0, $t7
	
	move $t7 , $v0	
	beq $t7,0x10000 segundamantisa1
	j unomantisa	
	
segundamantisa1:
	lw  $t7 8($a0)	
	beq $t7,0x00000 terceramantisa1
	j unomantisa		
	
terceramantisa1:
	lw  $t7 4($a0)	
	beq $t7,0x00000 cuartamantisa1
	j unomantisa	
	
cuartamantisa1:
	lw  $t7 0($a0)	
	beq $t7,0x00000 ceromantisa	
	j unomantisa
	
	

mantisados:

	lhu $t7,12($a1)
	addi $t7,$t7,0x00010000
	move $v1, $t7
	move $t7 , $v1	
	beq $t7,0x10000 segundamantisa2
	j unomantisa	
	
segundamantisa2:
	lw  $t7 8($a1)	
	beq $t7,0x00000 terceramantisa2
	j unomantisa		
	
terceramantisa2:
	lw  $t7 4($a1)	
	beq $t7,0x00000 cuartamantisa2
	j unomantisa		
	
cuartamantisa2:
	lw  $t7 0($a1)	
	beq $t7,0x00000 ceromantisa		
	j unomantisa
	
ceromantisa:
	move $v0,$zero 
	jr $ra 
	
unomantisa:
	li $v0, 1
	jr $ra 
	
unoexponente:
	li $v0, 1
	jr $ra 
ceroexponente:
	move $v0,$zero 
	jr $ra 
	
sumar:
	li $v0,4
	syscall

#excepciones 

excepciones:

	move $t4, $s0 #signo primero
	move $t0, $s5 #expo primero
	move $t2, $s6 #mantiza primero

	move $t5, $s1 #signo segundo
	move $t1, $s7 #expo segundo
	move $t3, $s4 #mantiza segundo

#CREACI�N DE NUMEROS

# +0=0
# -0=1
# +inf=2
# -inf=3
# NAN=4

Nuevonumero1:
	beqz $t4, expo10           #si t0 = 0 va a a expo10
	beqz $t0, analizarMantisa # 1,0,?
	beqz $t2, menosInf1  # 1,1,0
	j NAN1   # 1,1,1
	
expo10:
	beqz $t0, analizarMantisa1 
	beqz $t2, masInf1 
	j NAN1      # 0,1,1
	
analizarMantisa: # 1,0,?
	beqz $t2, menosCero1 # 1,0,0
	j numeroNormal1

analizarMantisa1: # 0,0,?
	beqz $t2, masCero1 # 0,0,0
	j numeroNormal1

NAN1:
	li $t8, 4 # hace NAN porque 1,1,1
	j NuevoNumero2
		
menosInf1:
	li $t8, 3
	j NuevoNumero2

masInf1:
	li $t8,2
	j NuevoNumero2

masCero1:
	li $t8, 0
	j NuevoNumero2

menosCero1:
	li $t8, 1
	j NuevoNumero2
	
numeroNormal1:
	li $t8, 5
	j NuevoNumero2


NuevoNumero2:
	beqz $t5, expo2 #si t0 = 0 e va  aanalizarExpo
	beqz $t1, analizarMantisa2 # 1,0,?
	beqz $t3, menosInf2  # se va a hacer menosInf1 porque 1,1,0
	j NAN2    # 1,1,1
	
expo2: 
	beqz $t1, analizarMantisa22 #analizarmantiza.
	beqz $t3, masInf2 
	j NAN2      # 0,1,1
	
analizarMantisa2: # 1,0,?
	beqz $t3, menosCero2 # 1,0,0
	j numeroNormal2

analizarMantisa22: # 0,0,?
	beqz $t3, masCero2 # 0,0,0
	j numeroNormal2

NAN2:
	li $t9, 4 # hace NAN porque 1,1,1
	j sumaEspecial
		
menosInf2:
	li $t9, 3
	j sumaEspecial

masInf2:
	li $t9,2
	j sumaEspecial

masCero2:
	li $t9, 0
	j sumaEspecial

menosCero2:
	li $t9, 1
	j sumaEspecial
	
numeroNormal2:
	li $t9, 5
	j sumaEspecial

sumaEspecial:
	
	beq $t8,4,esNAN
	beq $t9,4,esNAN
	
	beq $t8,0,primeroMasCero
	beq $t8,1,primeroMenosCero
	beq $t8,2,primeroMasInf
	beq $t8,3,primeroMenosInf
	beq $t8,5,primeroNormal
	
	
primeroMasCero:
	beq $t9,0, esMasCero
	beq $t9,1, esMenosCero
	beq $t9,2, esMasInf
	beq $t9,3, esMenosInf
	beq $t9,5, esNormal 
	

primeroMenosCero:
	beq $t9,0,esMenosCero
	beq $t9,1,esMasCero
	beq $t9,2,esMasInf
	beq $t9,3,esMenosInf
	beq $t9,5,esNormal 
	
primeroMasInf:
	beq $t9,0,esMasInf
	beq $t9,1,esMasInf
	beq $t9,2,esMasInf
	beq $t9,3,esNAN
	beq $t9,5,esMasInf
	
primeroMenosInf:
	beq $t9,0,esMenosInf
	beq $t9,1,esMenosInf
	beq $t9,3,esMenosInf
	beq $t9,2,esNAN
	beq $t9,5,esMenosInf

primeroNormal:
	beq $t9,0,esNormal 
	beq $t9,1,esNormal 
	beq $t9,2,esMasInf
	beq $t9,3,esMenosInf
	beq $t9,4,esNAN
	
esMasCero:
	li $v1, 0
	lw $t0,0($sp)
	jr $t0

esMenosCero:
	li $v1, 1
	lw $t0,0($sp)
	jr $t0

esMasInf:
	li $v1, 2
	lw $t0,0($sp)
	jr $t0
	
esMenosInf:
	li $v1, 3
	lw $t0,0($sp)
	jr $t0

esNAN:
	li $v1, 4
	lw $t0,0($sp)
	jr $t0

esNormal:
	li $v1,5
	lw $t0,0($sp)
	addi $sp, $sp,4
	jr $t0

salida:
	li $v0 10
	syscall
	
	
