# end prelude first file
# prelude second file
# data first file
.data
__FPZERO:
.word 0x00000000, 0x00000000, 0x00000000, 0x00000000
__FPZERONEG:
.word 0x00000000, 0x00000000, 0x00000000, 0x80000000
__FPINFYNEG:
.word 0x00000000, 0x00000000, 0x00000000, 0xFFFF0000
__FPINFY:
.word 0x00000000, 0x00000000, 0x00000000, 0x7FFF0000
__FPNANNEG:
.word 0x00000000, 0x00000000, 0x00000000, 0xFFFF1000
__FPNANPOS:
.word 0x00000000, 0x00000000, 0x00000000, 0x7FFF1000
__FPONE:
.word 0x00000000, 0x00000000, 0x00000000, 0x3FFF0000
__operator1:
.word 0x00000000, 0x00000000, 0x00000000, 0x00000000
__operator2:
.word 0x00000000, 0x00000000, 0x00000000, 0x00000000
__result:
.word 0x00000000, 0x00000000, 0x00000000, 0x00000000
__ttest:
.asciiz "Test "
__tdots:
.asciiz ": "
__resultFile:
.asciiz "Result.dat"
.asciiz "\n "
__sErrorOpenMessage:
.asciiz "Error opening file\n2"
__FD:
.word 0x00000000
# end data first file
# data second file


op1:  .word 0x00000000 0x00000000 0x00000000 0x7fff0000
op2:  .word 0x00000000 0x00000000 0x00000000 0xFfff0000
resul: .space 512


# text first file
.text
# entry point
# main
# s0: operator1
# s1: operator2
main:
	jal   __openFD
	li    $t0, -1
	beq   $v0, $t0, __errOpen
	la    $t0, __FD
	sw    $v0, 0($t0)
	j     __test1
# begin __test1
__test1:
# Print values
	la    $a0, __FPZERO
	jal   __appendFP128ToResultFile
	la    $a0, __FPZERO
	jal   __appendFP128ToResultFile
	la    $a0, __FPZERO
	la    $a1, __operator1
	jal   __copyFPNumbers
	la    $a0, __FPZERO
	la    $a1, __operator2
	jal   __copyFPNumbers
	la    $a0, __operator1
	la    $a1, __operator2
	la    $a2, __result
	jal   sumarFP128
	la    $a0, __result
	jal   __appendFP128ToResultFile
	j     __test2
#end __test1
#begin_test2
__test2:
# Print values
	la    $a0, __FPINFY
	jal   __appendFP128ToResultFile
	la    $a0, __FPZERO
	jal   __appendFP128ToResultFile
	la    $a0, __FPINFY
	la    $a1, __operator1
	jal   __copyFPNumbers
	la    $a0, __FPZERO
	la    $a1, __operator2
	jal   __copyFPNumbers
	la    $a0, __operator1
	la    $a1, __operator2
	la    $a2, __result
	jal   sumarFP128
	la    $a0, __result
	jal   __appendFP128ToResultFile
	j     __test3
#end __test2
#begin_test3
__test3:
# Print values
	la    $a0, __FPZERO
	jal   __appendFP128ToResultFile
	la    $a0, __FPINFY
	jal   __appendFP128ToResultFile
	la    $a0, __FPZERO
	la    $a1, __operator1
	jal   __copyFPNumbers
	la    $a0, __FPINFY
	la    $a1, __operator2
	jal   __copyFPNumbers
	la    $a0, __operator1
	la    $a1, __operator2
	la    $a2, __result
	jal   sumarFP128
	la    $a0, __result
	jal   __appendFP128ToResultFile
	j     __test4
#end __test3
#begin_test4
__test4:
# Print values
	la    $a0, __FPZERO
	jal   __appendFP128ToResultFile
	la    $a0, __FPNANPOS
	jal   __appendFP128ToResultFile
	la    $a0, __FPZERO
	la    $a1, __operator1
	jal   __copyFPNumbers
	la    $a0, __FPNANPOS
	la    $a1, __operator2
	jal   __copyFPNumbers
	la    $a0, __operator1
	la    $a1, __operator2
	la    $a2, __result
	jal   sumarFP128
	la    $a0, __result
	jal   __appendFP128ToResultFile
	j     __test5
#end __test4
#begin_test5
__test5:
# Print values
	la    $a0, __FPINFY
	jal   __appendFP128ToResultFile
	la    $a0, __FPNANPOS
	jal   __appendFP128ToResultFile
	la    $a0, __FPINFY
	la    $a1, __operator1
	jal   __copyFPNumbers
	la    $a0, __FPNANPOS
	la    $a1, __operator2
	jal   __copyFPNumbers
	la    $a0, __operator1
	la    $a1, __operator2
	la    $a2, __result
	jal   sumarFP128
	la    $a0, __result
	jal   __appendFP128ToResultFile
	j     __test6
#end __test5
#begin_test6
__test6:
# Print values
	la    $a0, __FPONE
	jal   __appendFP128ToResultFile
	la    $a0, __FPONE
	jal   __appendFP128ToResultFile
	la    $a0, __FPONE
	la    $a1, __operator1
	jal   __copyFPNumbers
	la    $a0, __FPONE
	la    $a1, __operator2
	jal   __copyFPNumbers
	la    $a0, __operator1
	la    $a1, __operator2
	la    $a2, __result
	jal   sumarFP128
	la    $a0, __result
	jal   __appendFP128ToResultFile
	j     __test7
#end __test6
#begin_test7
__test7:
# Print values
	la    $a0, __FPONE
	jal   __appendFP128ToResultFile
	la    $a0, __FPINFY
	jal   __appendFP128ToResultFile
	la    $a0, __FPONE
	la    $a1, __operator1
	jal   __copyFPNumbers
	la    $a0, __FPINFY
	la    $a1, __operator2
	jal   __copyFPNumbers
	la    $a0, __operator1
	la    $a1, __operator2
	la    $a2, __result
	jal   sumarFP128
	la    $a0, __result
	jal   __appendFP128ToResultFile
	j     __test8
#end __test7
#begin_test8
__test8:
# Print values
	la    $a0, __FPONE
	jal   __appendFP128ToResultFile
	la    $a0, __FPNANPOS
	jal   __appendFP128ToResultFile
	la    $a0, __FPONE
	la    $a1, __operator1
	jal   __copyFPNumbers
	la    $a0, __FPNANPOS
	la    $a1, __operator2
	jal   __copyFPNumbers
	la    $a0, __operator1
	la    $a1, __operator2
	la    $a2, __result
	jal   sumarFP128
	la    $a0, __result
	jal   __appendFP128ToResultFile
	j     __test9
#end __test8

#begin_test9
__test9:
# Print values
	la    $a0, __FPINFY
	jal   __appendFP128ToResultFile
	la    $a0, __FPINFY
	jal   __appendFP128ToResultFile
	la    $a0, __FPINFY
	la    $a1, __operator1
	jal   __copyFPNumbers
	la    $a0, __FPINFY
	la    $a1, __operator2
	jal   __copyFPNumbers
	la    $a0, __operator1
	la    $a1, __operator2
	la    $a2, __result
	jal   sumarFP128
	la    $a0, __result
	jal   __appendFP128ToResultFile
	j     __test10
#end __test9


#begin_test10
__test10:
# Print values
	la    $a0, __FPNANPOS
	jal   __appendFP128ToResultFile
	la    $a0, __FPNANPOS
	jal   __appendFP128ToResultFile
	la    $a0, __FPNANPOS
	la    $a1, __operator1
	jal   __copyFPNumbers
	la    $a0, __FPNANPOS
	la    $a1, __operator2
	jal   __copyFPNumbers
	la    $a0, __operator1
	la    $a1, __operator2
	la    $a2, __result
	jal   sumarFP128
	la    $a0, __result
	jal   __appendFP128ToResultFile
	j     __endMain
#end __test10

__errOpen:
	li       $v0, 4
	la       $a0, __sErrorOpenMessage
	syscall
__endMain:
	li    $v0, 10
	syscall
# end main
#
__openFD:
	addi     $sp, $sp, -12
	sw       $a2, 8($sp)
	sw       $a1, 4($sp)
	sw       $a0, 0($sp)
	li       $v0, 13
	la       $a0, __resultFile
	li       $a1, 1
	li       $a2, 0
	syscall
	sw       $a0, 0($sp)
	sw       $a1, 4($sp)
	sw       $a2, 8($sp)
	jr       $ra
# __appendFP128ToResultFile
# $a0: PFP128 number
# $s0: FD result file
# $s1: index
# $s2: address base
__appendFP128ToResultFile:
	addi     $sp, $sp, -12
	sw       $a2, 8($sp)  #
	sw       $s1, 4($sp)  # operator2
	sw       $s0, 0($sp)  # operator1
	move     $s2, $a0
	la       $t0, __FD
	lw       $s0, 0($t0)
	move     $s1, $zero
# loop
__loop:
	li       $t1, 4
	beq      $s1, $t1, __endloop
	move     $t0, $s1
	sll      $t0, $t0, 2
	add      $t0, $t0, $s2
	li       $v0, 15
	move     $a0, $s0
	move     $a1, $t0
	li       $a2, 4
	syscall
	addi     $s1, $s1, 1
	j        __loop
__endloop:
	lw       $s0, 0($sp)  # operator1
	lw       $s1, 4($sp)  # operator2
	lw       $s2, 8($sp)  # openFile
	addi     $sp, $sp, 12
	jr       $ra
# end __appendFP128ToResultFile
#
# __copyFPNumbers:
# $a0 = src
# $a1 = dest
# $s0 = index
# $t0 = aux
# $t1 = real address src
# $t2 = real address dest
# $t3 = tmp
__copyFPNumbers:
	addi     $sp, $sp, -4
	sw       $s0, 0($sp)
	move     $s0, $zero
__cfpLoop:
	li       $t1, 4
	beq      $s0, $t1,__cfpEnd
	move     $t0, $s0
	sll      $t0, $t0, 2
	move     $t1, $a0
	add      $t1, $t1, $t0
	move     $t2, $a1
	add      $t2, $t2, $t0
	lw       $t3, 0($t1)
	sw       $t3, 0($t2)
	addi     $s0, $s0, 1
	j        __cfpLoop
__cfpEnd:
	lw       $s0, 0($sp)
	addi     $sp, $sp, 4
	jr       $ra
#
#
# End part of the first file
# Rest of the second file


_main:
	la $a0,op1
	la $a1,op2
	la $a2,resul
	jal suma
	j salida


mantisa:
	lhu $t0,12($a0)
	addi $t0,$t0,0x00010000
	move $v0, $t0
	lhu $t0,12($a1)
	addi $t0,$t0,0x00010000
	move $v1, $t0
	jr $ra

exponente:
	lhu $t1,14($a0)
	lhu $t2,14($a1)
	li $t3 0x7fff
	and $t5 $t3 $t1
	move $v0 $t5
	and $t6 $t3 $t2
	move $v1 $t6
	jr $ra
signo:
	lbu $t1,15($a0)
	lbu $t2,15($a1)
	srl $t4,$t1,7
	srl $t5,$t2,7
	move $v0 $t4
	move $v1 $t5
	jr $ra

sumarFP128:
suma:
	addi $sp $sp -4
	sw	$ra 0($sp)

        jal signo
	move $s0, $v0         #Primero
	move $s1, $v1	      #segundo

	jal exponente
	move $s2, $v0         #primero
	move $s3, $v1	      #segundo


	jal expouno
	move $s5, $v0
	jal mantisauno
	move $s6, $v0

	jal expodos
	move $s7, $v0
	jal mantisados
	move $s4, $v0

	j excepciones

	expouno:
	beq $s2,0x7fff 	unoexponente
	beq $s2,0x0000  ceroexponente
	li $t0, 2

	expodos:
	beq $s3,0x7fff unoexponente
	beq $s3,0x0000 ceroexponente
	li $t1, 2

mantisauno:

	lhu $t7,12($a0)
	addi $t7,$t7,0x00010000
	move $v0, $t7

	move $t7 , $v0
	beq $t7,0x10000 segundamantisa1
	j unomantisa

segundamantisa1:
	lw  $t7 8($a0)
	beq $t7,0x00000 terceramantisa1
	j unomantisa

terceramantisa1:
	lw  $t7 4($a0)
	beq $t7,0x00000 cuartamantisa1
	j unomantisa

cuartamantisa1:
	lw  $t7 0($a0)
	beq $t7,0x00000 ceromantisa
	j unomantisa



mantisados:

	lhu $t7,12($a1)
	addi $t7,$t7,0x00010000
	move $v1, $t7
	move $t7 , $v1
	beq $t7,0x10000 segundamantisa2
	j unomantisa

segundamantisa2:
	lw  $t7 8($a1)
	beq $t7,0x00000 terceramantisa2
	j unomantisa

terceramantisa2:
	lw  $t7 4($a1)
	beq $t7,0x00000 cuartamantisa2
	j unomantisa

cuartamantisa2:
	lw  $t7 0($a1)
	beq $t7,0x00000 ceromantisa
	j unomantisa

ceromantisa:
	move $v0,$zero
	jr $ra

unomantisa:
	li $v0, 1
	jr $ra

unoexponente:
	li $v0, 1
	jr $ra
ceroexponente:
	move $v0,$zero
	jr $ra

sumar:
	li $v0,4
	syscall

#excepciones

excepciones:

	move $t4, $s0 #signo primero
	move $t0, $s5 #expo primero
	move $t2, $s6 #mantiza primero

	move $t5, $s1 #signo segundo
	move $t1, $s7 #expo segundo
	move $t3, $s4 #mantiza segundo

#CREACI�N DE NUMEROS

# +0=0
# -0=1
# +inf=2
# -inf=3
# NAN=4

Nuevonumero1:
	beqz $t4, expo10           #si t0 = 0 va a a expo10
	beqz $t0, analizarMantisa # 1,0,?
	beqz $t2, menosInf1  # 1,1,0
	j NAN1   # 1,1,1

expo10:
	beqz $t0, analizarMantisa1
	beqz $t2, masInf1
	j NAN1      # 0,1,1

analizarMantisa: # 1,0,?
	beqz $t2, menosCero1 # 1,0,0
	j numeroNormal1

analizarMantisa1: # 0,0,?
	beqz $t2, masCero1 # 0,0,0
	j numeroNormal1

NAN1:
	li $t8, 4 # hace NAN porque 1,1,1
	j NuevoNumero2

menosInf1:
	li $t8, 3
	j NuevoNumero2

masInf1:
	li $t8,2
	j NuevoNumero2

masCero1:
	li $t8, 0
	j NuevoNumero2

menosCero1:
	li $t8, 1
	j NuevoNumero2

numeroNormal1:
	li $t8, 5
	j NuevoNumero2


NuevoNumero2:
	beqz $t5, expo2 #si t0 = 0 e va  aanalizarExpo
	beqz $t1, analizarMantisa2 # 1,0,?
	beqz $t3, menosInf2  # se va a hacer menosInf1 porque 1,1,0
	j NAN2    # 1,1,1

expo2:
	beqz $t1, analizarMantisa22 #analizarmantiza.
	beqz $t3, masInf2
	j NAN2      # 0,1,1

analizarMantisa2: # 1,0,?
	beqz $t3, menosCero2 # 1,0,0
	j numeroNormal2

analizarMantisa22: # 0,0,?
	beqz $t3, masCero2 # 0,0,0
	j numeroNormal2

NAN2:
	li $t9, 4 # hace NAN porque 1,1,1
	j sumaEspecial

menosInf2:
	li $t9, 3
	j sumaEspecial

masInf2:
	li $t9,2
	j sumaEspecial

masCero2:
	li $t9, 0
	j sumaEspecial

menosCero2:
	li $t9, 1
	j sumaEspecial

numeroNormal2:
	li $t9, 5
	j sumaEspecial

sumaEspecial:

	beq $t8,4,esNAN
	beq $t9,4,esNAN

	beq $t8,0,primeroMasCero
	beq $t8,1,primeroMenosCero
	beq $t8,2,primeroMasInf
	beq $t8,3,primeroMenosInf
	beq $t8,5,primeroNormal


primeroMasCero:
	beq $t9,0, esMasCero
	beq $t9,1, esMenosCero
	beq $t9,2, esMasInf
	beq $t9,3, esMenosInf
	beq $t9,5, esNormal


primeroMenosCero:
	beq $t9,0,esMenosCero
	beq $t9,1,esMasCero
	beq $t9,2,esMasInf
	beq $t9,3,esMenosInf
	beq $t9,5,esNormal

primeroMasInf:
	beq $t9,0,esMasInf
	beq $t9,1,esMasInf
	beq $t9,2,esMasInf
	beq $t9,3,esNAN
	beq $t9,5,esMasInf

primeroMenosInf:
	beq $t9,0,esMenosInf
	beq $t9,1,esMenosInf
	beq $t9,3,esMenosInf
	beq $t9,2,esNAN
	beq $t9,5,esMenosInf

primeroNormal:
	beq $t9,0,esNormal
	beq $t9,1,esNormal
	beq $t9,2,esMasInf
	beq $t9,3,esMenosInf
	beq $t9,4,esNAN

esMasCero:
	li $v1, 0
	lw $t0,0($sp)
	jr $t0

esMenosCero:
	li $v1, 1
	lw $t0,0($sp)
	jr $t0

esMasInf:
	li $v1, 2
	lw $t0,0($sp)
	jr $t0

esMenosInf:
	li $v1, 3
	lw $t0,0($sp)
	jr $t0

esNAN:
	li $v1, 4
	lw $t0,0($sp)
	jr $t0

esNormal:
	li $v1,5
	lw $t0,0($sp)
	addi $sp, $sp,4
	jr $t0

salida:
	li $v0 10
	syscall


